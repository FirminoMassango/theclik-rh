package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ferias.class)
public abstract class Ferias_ {

	public static volatile SingularAttribute<Ferias, Integer> diasDeFerias;
	public static volatile SingularAttribute<Ferias, String> observacoes;
	public static volatile SingularAttribute<Ferias, String> apelido;
	public static volatile SingularAttribute<Ferias, Date> dataFim;
	public static volatile SingularAttribute<Ferias, String> nome;
	public static volatile SingularAttribute<Ferias, Integer> id;
	public static volatile SingularAttribute<Ferias, Date> dataInicio;
	public static volatile SingularAttribute<Ferias, Boolean> deFerias;
	public static volatile SingularAttribute<Ferias, Date> dataActual;

}

