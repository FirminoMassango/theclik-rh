package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Funcionario.class)
public abstract class Funcionario_ {

	public static volatile SingularAttribute<Funcionario, String> competente;
	public static volatile SingularAttribute<Funcionario, Integer> nuit;
	public static volatile SingularAttribute<Funcionario, String> estado;
	public static volatile SingularAttribute<Funcionario, String> motivo;
	public static volatile SingularAttribute<Funcionario, String> apelido;
	public static volatile SingularAttribute<Funcionario, String> bi;
	public static volatile SingularAttribute<Funcionario, String> instituicao;
	public static volatile SingularAttribute<Funcionario, String> numeroTelefone;
	public static volatile SingularAttribute<Funcionario, Double> salarioBase;
	public static volatile SingularAttribute<Funcionario, String> dedicado;
	public static volatile SingularAttribute<Funcionario, String> obsFalta;
	public static volatile SingularAttribute<Funcionario, String> genero;
	public static volatile SingularAttribute<Funcionario, Integer> id;
	public static volatile SingularAttribute<Funcionario, Date> dataNascimento;
	public static volatile SingularAttribute<Funcionario, String> nivelAcademico;
	public static volatile SingularAttribute<Funcionario, Integer> nrFaltas;
	public static volatile SingularAttribute<Funcionario, String> email;
	public static volatile SingularAttribute<Funcionario, Date> dataFimDoContracto;
	public static volatile SingularAttribute<Funcionario, String> endereco;
	public static volatile SingularAttribute<Funcionario, String> profissao;
	public static volatile SingularAttribute<Funcionario, String> nome;
	public static volatile SingularAttribute<Funcionario, String> formacaoAdequadaAoCargo;
	public static volatile SingularAttribute<Funcionario, String> flexivel;
	public static volatile SingularAttribute<Funcionario, String> disciplinado;
	public static volatile SingularAttribute<Funcionario, String> tipoDeAdmissao;
	public static volatile SingularAttribute<Funcionario, Date> dataIngresso;
	public static volatile SingularAttribute<Funcionario, Boolean> ferias;

}

