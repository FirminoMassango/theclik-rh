package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Salario.class)
public abstract class Salario_ {

	public static volatile SingularAttribute<Salario, Double> mecanico;
	public static volatile SingularAttribute<Salario, Double> electricista;
	public static volatile SingularAttribute<Salario, Double> motorista;
	public static volatile SingularAttribute<Salario, Double> cozinheiro;
	public static volatile SingularAttribute<Salario, Integer> id;
	public static volatile SingularAttribute<Salario, Double> guarda;
	public static volatile SingularAttribute<Salario, Double> frentista;
	public static volatile SingularAttribute<Salario, Double> contabilista;
	public static volatile SingularAttribute<Salario, Double> serrelheiro;

}

