package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Remuneracao.class)
public abstract class Remuneracao_ {

	public static volatile SingularAttribute<Remuneracao, Double> subsidioDecimoTerceiro;
	public static volatile SingularAttribute<Remuneracao, Double> subsidioDeFerias;
	public static volatile SingularAttribute<Remuneracao, Double> subsidioDeTransporte;
	public static volatile SingularAttribute<Remuneracao, Double> subsidioDeCargo;
	public static volatile SingularAttribute<Remuneracao, Double> bonus;
	public static volatile SingularAttribute<Remuneracao, Double> salarioBase;
	public static volatile SingularAttribute<Remuneracao, Integer> id;
	public static volatile SingularAttribute<Remuneracao, Double> subsidioDeAlimentacao;
	public static volatile SingularAttribute<Remuneracao, Double> aumentoSalarial;
	public static volatile SingularAttribute<Remuneracao, Double> descontoSalarial;

}

